#!/usr/bin/env lua
-- encoding: utf-8 --
------------------------------------------------------------------------
-- Este código-fonte pode ser convertido para Windows-1252 para utilizar
-- as entradas codificadas em Windows-1252.
------------------------------------------------------------------------

local num2ba = {[0] = 'J','A','B','C','D','E','F','G','H','I'}

local pont2ba = {
	-- ASCII compatível:
	['.'] = "'", [','] = "1", [';'] = "2", [':'] = "3",
	['?'] = "5", ['!'] = "6",
	['('] = "<'", [')'] = ",>", ['['] = "('", [']'] = ",)",
		-- Nota de transcrição: "|[" e "|]".
	['"'] = "8", ['\''] = ",8",
	['/'] = ",1", ['|'] = "_",
	['*'] = "9", ['#'] = "#K", ['$'] = ";", ['%'] = "_0",
	['+'] = "6", ['<'] = "[", ['='] = "7", ['>'] = "O",

	-- Conjunto de caracteres Latin-1 (ISO-8859-1 e Windows-1252):
	[' '] = " ", -- Espaço não quebrável.
	['ª'] = "A", ['º'] = "O", -- Para símbolo de gênero: "<ow," e "<o->".
	['«'] = ";8", ['»'] = ";8",
	['§'] = "SS", ['©'] = "<.C>", ['®'] = "<.R>",
	['£'] = "@L", ['¥'] = "@Y",
	['°'] = "0", -- Para minuto e segundo: \ e "\\".
	['¹'] = "*#A", ['²'] = "*#B", ['³'] = "*#C", -- Prefixo â. Subscrito: í.
	['×'] = "8", ['÷'] = "4",
	['¼'] = "#1D", ['½'] = "#1B", ['¾'] = "#3D",
	['µ'] = "@M",

	-- Conjunto de caracteres Windows-1252 (não é compatível a ISO-8859-1):
	['…'] = "'''", ['—'] = "--", -- Equiv.: "..." e "--".
	['“'] = "8", ['”'] = "8", ['‘'] = ",8", ['’'] = ",8", -- Equiv.: " e '.
	['‹'] = ";8", ['›'] = ";8", -- Equiv.: « e ».
	['†'] = "W1", -- Equiv.: "w," e Õ (estrela).
	['•'] = "[O", -- Equiv.: ("<>" ou "<o") e "|y" (quadrado).
	['€'] = "@E", ['‰'] = "_00", -- Equiv.: "^e" e "%°".

	-- Acentos:
	['^'] = "@", ['`'] = "5", ['~'] = "\"", ['¨'] = "^", ['´'] = "9",
	['ˆ'] = "@", ['˜'] = "\"", -- Não é compatível a ISO-8859-1, utilizar ^ e ~.

	-- Letras maiúsculas gregas (UTF-8 somente):
	['\xCE\x91'] = "@A", ['\xCE\x92'] = "@B", ['\xCE\x93'] = "@G",
	['\xCE\x94'] = "@D", ['\xCE\x95'] = "@E", ['\xCE\x96'] = "@Z",
	['\xCE\x97'] = "@:", ['\xCE\x98'] = "@?", ['\xCE\x99'] = "@I",
	['\xCE\x9A'] = "@K", ['\xCE\x9B'] = "@L", ['\xCE\x9C'] = "@M",
	['\xCE\x9D'] = "@N", ['\xCE\x9E'] = "@X", ['\xCE\x9F'] = "@O",
	['\xCE\xA0'] = "@P", ['\xCE\xA1'] = "@R", ['\xCE\xA3'] = "@S",
	['\xCE\xA4'] = "@T", ['\xCE\xA5'] = "@U", ['\xCE\xA6'] = "@F",
	['\xCE\xA7'] = "@C", ['\xCE\xA8'] = "@Y", ['\xCE\xA9'] = "^W",
	-- Letras minúsculas gregas (UTF-8 somente):
	['\xCE\xB1'] = "@A", ['\xCE\xB2'] = "@B", ['\xCE\xB3'] = "@G",
	['\xCE\xB4'] = "@D", ['\xCE\xB5'] = "@E", ['\xCE\xB6'] = "@Z",
	['\xCE\xB7'] = "@:", ['\xCE\xB8'] = "@?", ['\xCE\xB9'] = "@I",
	['\xCE\xBA'] = "@K", ['\xCE\xBB'] = "@L", ['\xCE\xBC'] = "@M",
	['\xCE\xBD'] = "@N", ['\xCE\xBE'] = "@X", ['\xCE\xBF'] = "@O",
	['\xCF\x80'] = "@P", ['\xCF\x81'] = "@R", ['\xCF\x83'] = "@S",
	['\xCF\x84'] = "@T", ['\xCF\x85'] = "@U", ['\xCF\x86'] = "@F",
	['\xCF\x87'] = "@C", ['\xCF\x88'] = "@Y", ['\xCF\x89'] = "@W",

	-- Contexto informático ("~`"):
	info = {
		['@'] = ":", ['&'] = "\"&",
		['_'] = ".-", ['/'] = "4", ['|'] = "_L", ['\\'] = "\"'",
		['\''] = ",",
		['('] = "\"<", [')'] = "\">", ['<'] = "\"[", ['>'] = "\"O",
		['['] = "\"(", [']'] = "\")", ['{'] = "\"L", ['}'] = "_1",
	},
}

local ac2ba = {
	b = {
		--['à'] = "$", ['è'] = "!", ['ì'] = "/", ['ò'] = "+", ['ù'] = ")",
		['à'] = "$", ['è'] = "!", ['ì'] = "%", ['ò'] = "W", ['ù'] = ":",
		['á'] = "(", ['é'] = "=", ['í'] = "/", ['ó'] = "+", ['ú'] = ")",
		['â'] = "*", ['ê'] = "<", ['î'] = "%", ['ô'] = "?", ['û'] = ":",
		['ã'] = ">",                           ['õ'] = "[", ['ñ'] = "]",
		['ä'] = ">", ['ë'] = "$", ['ï'] = "]", ['ö'] = "[", ['ü'] = "\\",
		['ç'] = "&",

		['ý'] = "9Y", ['ÿ'] = "^Y", ['š'] = "@S", ['ž'] = "@Z",
		-- Sueco, dinamarquês, alemão, francês únicos.
		['å'] = "*", -- Mesmo sinal de "â".
		['æ'] = ">", -- Mesmo sinal de "ã".
		['ø'] = "[", -- Mesmo sinal de "õ".
		['ß'] = "!", -- Sinal similar a "s".
		['œ'] = "[", -- Mesmo sinal de "õ".
	},
	a = {
		--['À'] = "$", ['È'] = "!", ['Ì'] = "/", ['Ò'] = "+", ['Ù'] = ")",
		['À'] = "$", ['È'] = "!", ['Ì'] = "%", ['Ò'] = "W", ['Ù'] = ":",
		['Á'] = "(", ['É'] = "=", ['Í'] = "/", ['Ó'] = "+", ['Ú'] = ")",
		['Â'] = "*", ['Ê'] = "<", ['Î'] = "%", ['Ô'] = "?", ['Û'] = ":",
		['Ã'] = ">",                           ['Õ'] = "[", ['Ñ'] = "]",
		['Ä'] = ">", ['Ë'] = "$", ['Ï'] = "]", ['Ö'] = "[", ['Ü'] = "\\",
		['Ç'] = "&",

		['Ý'] = "9Y", ['Ÿ'] = "^Y", ['Š'] = "@S", ['Ž'] = "@Z",
		-- Sueco, dinamarquês, francês únicos.
		['Å'] = "*", -- Mesmo sinal de "â".
		['Æ'] = ">", -- Mesmo sinal de "ã".
		['Ø'] = "[", -- Mesmo sinal de "õ".
		['Œ'] = "[", -- Mesmo sinal de "õ".
	},
}

local eminfo = false

local function tratapalavras(l)
	local out, i, anum, ncxa = "", 1, false, 0

	repeat
		local c = l:sub(i, i)
		if "°" == "\xC2\xB0" then -- Entrada UTF-8.
			c = l:match("^[\xC2-\xCF].", i) or l:match("^\xE2..", i) or c
		end

		local cxb = l:match("^%l+", i) or ac2ba.b[c] and c
		local cxa = l:match("^%u", i) or ac2ba.a[c] and c
		local num = l:match("^[%d%.]*%d,%d[%d%.]*", i) or
			l:match("^[%d%.]+%d%d%d", i) or l:match("^%d+", i)

		if cxb then
			out = out .. (ac2ba.b[cxb] or (
				(anum and cxb:match("^[a-j]") and (eminfo and ';' or '"') or "")
				.. cxb:upper()))
			i = i + #cxb
		elseif cxa then
			if ncxa == 1 then
				-- BUG: Não funciona se houver um padrão incomum: ABcd.
				out = out:gsub("%.[^%.]-$", ".%0")
			end
			out = (eminfo and out or out:gsub("_ $", "_")) ..
				(ncxa == 0 and "." or "") .. (ac2ba.a[cxa] or cxa)
			i = i + #cxa
		elseif num then
			-- Trata entre os parênteses e números.
			out = out:gsub("([<%(])'$", "%1") .. "#"
			local ord = l:match("^%d+ª", i) or l:match("^%d+º", i)
			if ord then
				out = out .. ord:gsub("%D+$", {['ª'] = "A", ['º'] = "O"})
				i = i + #ord
			else
				out = out .. num:gsub(".", function (c)
					return pont2ba[c] or num2ba[tonumber(c)]
				end)
				i = i + #num
			end
		else
			if c == '`' and i > 1 and l:sub(i - 1, i) == "~`" then
				eminfo = not eminfo
				out = out .. "1" -- "\"1"
			else
				out = out .. (eminfo and pont2ba.info[c] or pont2ba[c] or c)
				if not eminfo then
					-- Trata as notas de transição ("|[" e "|]").
					if c == '[' then
						out = out:gsub("_%('$", "_(")
					elseif c == ']' then
						out = out:gsub("_,%)$", "_)")
					elseif c == '|' then -- Remove o espaço antes de |.
						out = out:gsub(" _$", "_")
					elseif c == '-' or c == '>' then -- Setas ("<->").
						out = out:gsub("%[?%-O?$",
							{["-O"] = "3O", ["[-"] = "[3"})
					elseif c == "§" then -- Eliminação de duplo parágrafo.
						out = out:gsub("SSSS$", "SS")
					end
				end
				if c == ')' or c == ']' then
					-- Trata entre os parênteses e números.
					out = out:gsub("(#[A-J]+),([>%)])$", "%1%2")
						:gsub("(#%d+[AO]),([>%)])$", "%1%2")
						:gsub("([<%(])'(%.%.%u%u+),([>%)])$", "%1%2%3")
				end
			end
			i = i + #c
		end
		anum = num ~= nil
		ncxa = cxa and ncxa + 1 or 0
	until i > #l

	return out
end

if arg[1] == "-u" then -- Saída em Braille UTF-8.
	local ba2dots = {
		 0, 46, 16, 60, 43, 41, 47,  4, 55, 62, 33, 44, 32, 36, 40, 12, -- SP-/
		52,  2,  6, 18, 50, 34, 22, 54, 38, 20, 49, 48, 35, 63, 28, 57, -- 0-?
		 8,  1,  3,  9, 25, 17, 11, 27, 19, 10, 26,  5,  7, 13, 29, 21, -- @-O
		15, 31, 23, 14, 30, 37, 39, 58, 45, 61, 53, 42, 51, 59, 24, 56, -- P-_
	}

	for l in io.lines() do
		io.write(tratapalavras(l):gsub('[ -_]', function (c)
				return string.char(0xE2, 0xA0,
					ba2dots[c:byte() - string.byte(' ') + 1] + 128)
			end) .. '\n')
	end
else -- Saída em Braille ASCII.
	for l in io.lines() do
		io.write(tratapalavras(l) .. '\n')
	end
end
